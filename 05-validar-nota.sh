#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple if: nota aprovat o no
#	$prog edat
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS

fi
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
	echo "Error:NOTA $1 no esta en el rang"
	echo "Usage: un numero entre 0 i 10"
	exit $ERR_NOTA
fi

#2) xixa
nota=$1
if [ $nota -lt 5 ]
then
	echo "suspes"
else
	echo "aprovat"
fi
exit 0



