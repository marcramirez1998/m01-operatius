#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple if: indicar si és major d'edat
#	$prog edat
#----------------------------------------------------------

#1) Validar que existeix un argument
if [ $# -ne 1 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

#2) xixa
edat=$1
if [ $edat -lt 18 ]
then
	echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
	echo "edat $edat és de poblacio activa"
else
	echo "edat $edat és jubilat"
fi
exit 0
