#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple if: nota aprovat o no
#	$prog edat
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIR=2
if [ $# -ne 1 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 arguments"
	exit $ERR_NARGS

fi
if [ ! -d $1 ]
then
	echo "Error:Dir $1 no és un dir"
	echo "Usage: $0 dir"
	exit $ERR_DIR
fi

#2) xixa
fit=$1
if [ ! -e $fit  ]; then
  echo "$fit no existeix"
  exit $ERR_NOEXIST  
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -h $fit ]; then
  echo "$fit és un link"
elif [ -d $fit ]; then
  echo "$fit és un directori"
else
  echo "$fit és una altra cosa"	
fi
exit 0








