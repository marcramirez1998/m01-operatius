#! /bin/bash
# @marcramirez1998
# Març 2023
# 
# validar que es rep un arg i que es un directori i llistar-ne el contingut
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIR=2
if [ $# -ne 1 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 arguments"
	exit $ERR_NARGS

fi
#2) xixa
dir=$1
llista=$(ls $dir)
contador=1
for elem in $llista
do
	echo "$contador: $elem"
	contador=$((contador+1))
done
exit 0
