#! /bin/bash
# @marcramirez1998
# Febrer 2023
# programa que rep un numero...
# Exemples for
#----------------------------------------------------------

#10)sumador
suma=0
for num in $*
do
	suma=$((num+suma))
done
echo "la sima dels valos introduits es: $suma"
exit 0


#9) llista logins numerats
llista=$(cut -d: -f1 /etc/passwd | sort)
contador=1
for nom in $llista
do
	echo "$contador: $nom"
	((contador++))
done
exit 0



#8) nou (contador)
llista=$(ls)
contador=1
for nom in $llista
do
	echo "$contador: $nom"
	((contador++))
done
exit 0



#7) iterar i mostrar la llista amb un numero que suam cada cop 1 (contador)
contador=1
for arg in $*
do
	echo "$contador: $arg"
	contador=$((contador+1))
done
exit 0





#6) iterar i mostrar la llista d'arguments encapsulats

for arg in "$@"
do
	echo "$arg"

done
exit 0




#5) iterar i mostrar la llista d'arguments encapsulats

for arg in "$*"
do
	echo "$arg"

done
exit 0





#4) iterar i mostrar la llista d'arguments

for arg in $*
do
	echo "$arg"

done
exit 0




#3) iterar per un valor d'una variable
llistat=$(ls)

for nom in $llistat
do
	echo "$nom"

done
exit 0



#2) iterar per un conjunt d'elements
for nom in "pere marta pau anna"
do
	echo "$nom"

done
exit 0

#1) iterar per un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do
	echo "$nom"

done
exit 0








