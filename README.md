#Scripts 2022-2023

## @edt ASIX-M01

Llistat d'exercicis de M01 operatius-scripts 2022-2023

Per baixar
```
git clone
```

Per pujar els apunts

```
git add . ; git commit -m "m01 exemple" ; git push
```
Capçalera

```
#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple de processar arguments
# Normes:
#	shebang (#!) indicia qui ha d'interpretar el fitxer
#	capçalera: descripció, data, autor
#----------------------------------------------------------

```
