#! /bin/bash
# @marcramirez1998
# Març 2023
# 
# validar que es rep un arg i que es un directori i llistar-ne el contingut
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIR=2
if [ $# -eq 0 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 arguments"
	exit $ERR_NARGS

fi
#2) xixa
dir=$1
for dir in $*
do
	if ! [ -d $dir ];
	then
		echo "Error: $dir no es un dir"  1>&2
	else
		dir=$1
		llista=$(ls $dir)
		echo "dir: $dir"
		for elem in $llista
		do
			if [ -h "$dir/$elem" ];
			then
				echo -e "\t$elem es un link"
			elif [ -f "$dir/$elem" ];
			then
				echo -e "\t$elem es un relgularfile"
			elif [ -d "$dir/$elem" ];
			then
				echo -e "\t$elem es un directori"
			else
				echo -e "\t$elem es un altre"
			fi
		done
	fi
		
done
exit 0
