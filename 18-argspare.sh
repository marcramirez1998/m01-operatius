#! /bin/bash # @edt ASIX M01-ISO Curs 2022-2023 
# prog [ -a -b- -c- -d- -n -f] arg[...] 
# separar en dues llistes args, opcions,fitxers,num 
# ------------------------------------------

opcions=""
arguments=""
fitxer=""
num=""
while [ -n "$1" ]
do
  case $1 in
  -[abcd]) opcions="$opcions $1";;
  -f) file="$opcions $2"
    shift;;
  -n)
    num="$opcions $2"
    shift;;
  *)
     arguments="$arguments $1";;
  esac
  shift
done
echo "opcions: $opcions"
echo "arguments: $arguments"
echo "fitxer: $fitxer"
echo "num: $num"
exit 0
