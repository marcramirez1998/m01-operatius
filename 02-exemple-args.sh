#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple de processar arguments
# Normes:
#	shebang (#!) indicia qui ha d'interpretar el fitxer
#	capçalera: descripció, data, autor
#----------------------------------------------------------
echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$0: ' $0
echo '$1: ' $1
echo '$2: ' $2
echo '$9: ' $9
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$
nom="puig"
echo "${nom}deworld"
