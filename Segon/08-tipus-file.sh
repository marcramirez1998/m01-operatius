#! /bin/bash 
# @marcramirez1998 
# Febrer 2024
# 
# Exemple: valida nota: suspes, aprovat
#	a) rep un argument
#	b) es del 0 al 10
#----------------------------------------------------------
#1) arguments incorrectes
ERR_NARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi
#2) validar nota
if [ ! -d $1 ]
then
	echo "Error:Dir $1 no és un dir"
	echo "Usage: $0 dir"
	exit $ERR_DIR
fi
if [ -e $1 ]
then
	echo " no existeix"
elif [ -f $1 ]
then
	echo " no es regular"
elif [ -h $1 ]
then
	echo "no es un link"
else
	echo " es una altre cosa"
fi
exit 0
