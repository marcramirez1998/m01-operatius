#! /bin/bash
# @marcramirez1998
# Febrer 2024
#
# Descripcio: dir els dies que te un mes.
# SYnopsis: prog mes
# 	a)validar rep un arg
# 	b)validar mes [1-12]
# 	c)xixa
#
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIA=2

mes=$1
#1) validar args
if [ $# -ne 1 ]
then
        echo "Error:numero arguments incorrecte"
        echo "Usage: $0 argument"
        exit $ERR_NARGS

fi

#2) validar mes
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no existeix"
  echo "Els mesos son del 1 fins el 12"
  echo "Usage: $0 mes"
  exit $ERR_DIA
fi
#3) xixa
case $mes in
	"2")
		dies=28
		;;
	"4"|"6"|"9"|"11")
		dies=30
		;;
	*)
		dies=31
		;;
esac
echo "el mes $mes te $dies dies"
exit 0
