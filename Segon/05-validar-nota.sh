#! /bin/bash 
# @marcramirez1998 
# Febrer 2024
# 
# Exemple: valida nota: suspes, aprovat
#	a) rep un argument
#	b) es del 0 al 10
#----------------------------------------------------------
#1) arguments incorrectes
ERR_NARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 edat"
	exit $ERR_NARGS
fi
#2) validar nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
	echo " Error: Nota $1 no esta en el rang de notas"
	exit $ERR_NOTA
fi
#3) xixa
nota=$1
if [ $1 -lt 5 ]
then
	echo "Nota: $nota suspes"
else
	echo "Nota: $nota aprovat"
fi
exit 0
