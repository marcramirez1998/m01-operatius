#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exemples 
#	case 	$1 
#---------------------------------------------------------
#programa dia laborable, festiu i altres
case $1 in
	"dl"|"dt"|"dc"|"dj"|"dv")
		echo "$1 es un dia laborable"
		;;
	"ds"|"dm")
		echo "$1 es un dia festiu"
		;;
	*)
		echo "$1 no es un dia"
		;;
esac
exit 0
#programa vocals, consonants i altres
case $1 in
	[aeiou])
		echo "$1 es una vocal"
		;;
	[qwrtypsdfghjklñzxcvbnm])
		echo "$1 es una consonant"
		;;
	*)
		echo "$1 es una altre cosa"
		;;
esac
exit 0
