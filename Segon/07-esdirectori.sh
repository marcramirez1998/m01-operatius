#! /bin/bash
# @marcramirez1998
# Febrer 2024
#
# Validar si es Directorii
#	a)verificar arfs
#	b) verificar si es dir
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIR=2
if [ $# -ne 1 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 arguments"
	exit $ERR_NARGS

fi
#2) xixa
if [ ! -d $1 ]
then
	echo " $1 no es un directori "
	exit $ERR_DIR
fi
ls -l $1
exit 0
