#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici8:  Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. 
#Si no existeix el mostra per stderr. 
##      bash $prog colord
#       	colord
#      	bash $prog a183077mr
#      		Error: l'usuari a183077mr no existeix
#----------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 1 ]
then
echo "Error: El programa ha de tenir exactament un argument"
echo "Usage: $0 num"
exit $ERR_NARGS
fi

max=$1
compt=1

read -r line
while [ $max -gt 0 ]
do
echo "$compt: $line"
read -r line
((compt++))
((max--))
done

exit 0
