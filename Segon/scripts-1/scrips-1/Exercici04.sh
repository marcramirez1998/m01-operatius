#! /bin/bash
# @marcramirez1998
# Febrer 2024
#
# Exercici4: Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
##      bash $prog 2 6 12
#               0el mes 2 te 28 dies
#		el mes 6 te 30 dies
#		el mes 12 te 31 dies
#----------------------------------------------------------
#1) arguments incorrectes
ERR_NARGS=1
ERR_DIA=2

mes=$1
#1) validar args
if ! [ $# -ge 1 ]
then
        echo "Error:numero arguments incorrecte"
        echo "Usage: $0 argument"
        exit $ERR_NARGS

fi
#2) xixa
for mes in $*
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]
	then
                echo "Error, mes $mes no existeix, els mesos son del 1 fins el 12" >&2
	else
		case $mes in
			"2")
				dies=28
				;;
			"4"|"6"|"9"|"11")
				dies=30
				;;
			*)
				dies=31
				;;
		esac
	fi
echo "el mes $mes te $dies dies"
done
exit 0

