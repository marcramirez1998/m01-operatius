#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici3: Fer un comptador des de zero fins al valor indicat per l’argument rebut.
##      bash $prog 3
#		0
#               1
#               2
#               3
#----------------------------------------------------------
num=0
nummax=$1
while [ $num -le $nummax ]
do
        echo "$num"
        ((num++))
done
exit 0
~                     
