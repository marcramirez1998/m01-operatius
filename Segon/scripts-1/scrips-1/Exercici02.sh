#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici2: Mostar els arguments rebuts línia a línia, tot numerànt-los.
##      bash $prog pere anna cots
#       	1: pere
#       	2: anna
#       	3: cots
#----------------------------------------------------------

comptador=1
for arg in $*
do
	echo "$comptador: $arg"
	((comptador++))
done
exit 0
