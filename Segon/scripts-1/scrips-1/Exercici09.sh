#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici9:  Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el
#		 sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra
#		 per stderr. 
##      bash $prog
#		colord
#       	colord
#      	bash $prog 
#      		a183077mr
#      		Error: l'usuari a183077mr no existeix
#----------------------------------------------------------

while read -r user
	do
	grep "^$user:" /etc/passwd >& /dev/null
	if [ $? -eq 0 ]
	then
		echo "$user"
	else
        	echo "Error: l'usuari "$user" no existeix" >&2
    	fi
done
exit 0

