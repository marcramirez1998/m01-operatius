#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici5: Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
##      bash $prog 
#               12345343
#               12345
#----------------------------------------------------------
while read -r line
do
        echo "$line" | cut -c1-50
done
exit 0

