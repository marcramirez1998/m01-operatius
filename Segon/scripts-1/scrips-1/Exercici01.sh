#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici1: Mostrar l’entrada estàndard numerant línia a línia
##	bash $prog
#		sdsd
#		1: sdsd
#		ad
#		2: ad
#----------------------------------------------------------
comptador=1
while read -r line
do
		echo "$comptador: $line"
		((comptador++))
done
exit 0

