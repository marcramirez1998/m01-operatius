#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici7: Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
#		mostra, si no no.
##      bash $prog 3
#		1234567
#		1234567
#		1234
#		12345678
#		12345678
#		12

#----------------------------------------------------------
while read -r line; do
	num=$(echo "$line" | wc -c)
    	if [ "$num" -gt 60 ] 
	then
        	echo "$line"
    fi
done
exit 0

~                     
