#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici6: Fer un programa que rep com a arguments noms de dies de la setmana i mostra
#		quants dies eren laborables i quants festius. Si l’argument no és un dia de la
#		setmana genera un error per stderr.
#			Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
#				Error: kakota no és un dia de la setmana
#				Error: sunday no és un dia de la setmana
#				Hi ha 4 dies laborables i 1 dies festius.
#----------------------------------------------------------
laborables=0
festius=0

while [[ $# -gt 0 ]]; do
    case "$1" in
        dilluns|dimarts|dimecres|dijous|divendres)
            laborables=$((laborables++))
            ;;
        dissabte|diumenge)
            festius=$((festius++))
            ;;
        *)
            echo "Error: $1 no és un dia de la setmana" >&2
            ;;
    esac
    shift
done

echo "Hi ha $laborables dies laborables i $festius dies festius."

