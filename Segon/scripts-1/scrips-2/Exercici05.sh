#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#-----------------------------------------
while read -r line
do
  chars=$(echo $line | wc -c)
  if [ $chars -lt 50 ]
  then
    echo $line
  fi
done
exit 0
