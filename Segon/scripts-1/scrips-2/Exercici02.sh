#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
#2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#	bash contador.sh hola mundo 1234  ga  12 casa gato perro
#		hi ha 6 de 3 o més caràcters
#-----------------------------------------
num=0
for args in $*
do
	echo $args | egrep '.{3}' &> /dev/null
	if [ $? -eq 0 ]
	then 
		((num++))
	fi
done
echo "hi ha $num de 3 o més caràcters"
exit 0

