#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## # Exercici3: Processar arguments que són matricules:
#a)             Llistar les vàlides, del tipus: 9999-AAA.
#b)             stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
#               número d’errors (de no vàlides).
#----------------------------------------------------------
cont=0
for matricula in "$@"
do
  echo "$matricula" | egrep "^[0-9]{4}-[A-Z]{3}$" 2> /dev/null
  if [ $? -ne 0 ]
  then
   echo "ERROR: $matricula matricula no valida"  >> /dev/stderr	  
   	((cont++))
  fi
done 
exit 0


            
