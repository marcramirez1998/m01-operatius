#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
#	Programa: prog.sh [ -r -m -c cognom -j -e edat ] arg...
#		Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors
#		corresponents.
#		No cal validar ni mostrar res!
#			$ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#			opcions «-r -j»
#			cognom «puig»
#			edat «18»
#			arguments «wheel postgres ldap»
#-----------------------------------------
cognom=""
edat=""
opcions=""
arguments=""
while [ -n "$1" ]
do
	case "$1" in
	-r|-m|-j)
		opcions="$opcions $1";;
	-c)
		cognom=$2
		shift;;
	-e)
		edat=$2
		shift;;	
	*)
		arguments="$arguments $1";;
	esac
	shift
done
echo "El cognom: $cognom"
echo "El edat: $edat"
echo "Les opcions: $opcions"
echo "Els arguments: $arguments"
