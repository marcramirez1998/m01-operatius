#!/bin/bash
## @marcramirez1998 
## Febrer 2024 
# Programa: Programa -h uid...
# 			Per a cada uid mostra la informació de l’usuari en format:
# 			logid(uid) gname home shell
#-----------------------------------------
status=0
if [ $# -eq 1 -a "$1" = "-h" ]; then
  echo "mostrant el help"
  exit 0
fi
if [ $# -eq 0 ]; then
  echo "error num args..."
  echo "usage..."
  exit 1
fi
for uid in $*
do 
  ulinia=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  if [ $? -eq 0 ]; then
    login=$(echo $ulinia | cut -d: -f1) 
    gid=$(echo $ulinia | cut -d: -f4) 
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    dirHome=$(echo $ulinia | cut -d: -f6) 
    shell=$(echo $ulinia | cut -d: -f7) 
    echo "$login($uid) $gname $dirHome $shell"
  else
    echo "error $uid inexistent" >> /dev/stderr
  fi
done
exit $status

