#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
#	Programa: Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en
#	format: gname: GNAME, gid: GID, users: USERS
#-----------------------------------------
status=0
while read -r gid
do 
  groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]; then
    gname=$(echo $groupLine | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    llistaUsers=$(echo $groupLine | cut -d: -f4 | tr '[a-z]' '[A-Z]')
    echo "gname: $gname, gid: $gid, users: $llistaUsers"
  else
    echo "Error $gid inexistent!" >> /dev/stderr    
    status=1
  fi    
done
exit $status
