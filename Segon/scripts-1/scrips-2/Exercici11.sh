#!/bin/bash
## @marcramirez1998 
## Febrer 2024 
# Programa: Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en
# format: gname: GNAME, gid: GID, users: USERS
#-----------------------------------------

status=0

# Verificar si hay al menos un argumento
if [ "$#" -eq 0 ]; then
    echo "Error: Se requiere al menos un GID como argumento" >&2
    exit 1
fi

for gid in "$@"; do
    groupLine=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
    if [ $? -eq 0 ]; then
        gname=$(echo "$groupLine" | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
        llistaUsers=$(echo "$groupLine" | cut -d: -f4 | tr ',' ' ')
        echo "gname: $gname, gid: $gid, users: $llistaUsers"
    else
        echo "Error: $gid no encontrado" >&2
        status=1
    fi
done

exit $status