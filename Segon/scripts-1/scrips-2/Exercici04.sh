#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
# Processar stdin cmostrant per stdout les línies numerades i en majúscules..
#-----------------------------------------
cont=0
while read -r line
do
  echo $cont $line | tr '[:lower:]' '[:upper:]'
  ((cont++))
done
exit 0

