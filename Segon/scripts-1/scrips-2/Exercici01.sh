#! /bin/bash 
## @marcramirez1998 
## Febrer 2024 
## Scripts 2
## # Exercici01:  Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
##      bash $prog 123 1234 123456 12 1 12345
#		1234
#		123456
#		12345
#--------------------------------------------------------------
for arg in $@
do
	echo "$arg" | grep -E ".{4,}"
done
exit 0

