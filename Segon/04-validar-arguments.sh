#! /bin/bash 
# @marcramirez1998 
# Febrer 2024
# 
# Exemple: valida que hi ha 2 args
#$prog nom edat 
#----------------------------------------------------------
#
#1) arguments incorrectes
if [ $# -ne 2 ] # valida not equal 2 (que tiene de ser 2 si no error)
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 nom edat"
	exit 1
fi
#2) xixa
echo "nom: $1"
echo "edat: $2"
exit 0

