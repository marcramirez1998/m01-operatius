#! /bin/bash
# @marcramirez1998
# Febrer 2023
# programa que rep un numero...
# Exemples for: itera per el nombre finit d'elements
#----------------------------------------------------------
#8)llista el login dels usuaris ordenats i numerats
login=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $login
do
        echo "$num: $elem"
        ((num++))
done
exit 0

#7) numeran las lineas
llistat=$(ls)
num=1
for elem in $llistat
do
        echo "$num: $elem"
	((num++))
done
exit 0

#6) iterar els valors d'un ls
llistat=$(ls)
for elem in $llistat
do
        echo "$elem"
done
exit 0
#5)numera args
num=1
for arg in $*
do
        echo "$num: $arg"
	((num++))
done
exit 0

exit 0
#4)"$@" expandeix si esta entre cometas "$*" no
for arg in "$@"
do
	echo "$arg"
done
exit 0
#3)iterrar per llista d'elements
for arg in $*
do
	echo "$arg"
done
exit 0

#2) iterar noms
for nom in "pere marta anna pau"
do
        echo "$nom"
done
exit 0

#1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0
