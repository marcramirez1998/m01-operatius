#! /bin/bash
# @marcramirez1998
# Març 2023
# 
# Exemples while
#----------------------------------------------------------

ERR_NARGS=1

#7) numera entrada estandar i mostra en mayus l'entrada estandard
num=1
while read -r line
do
		echo "$num: $line" | tr 'a-z' 'A-Z'
		((num++))
done
exit 0




#6)mostrar stdin linia a lina fins tokein FI
read -r line 
while [ "$line" != "fi" ]
do
		echo "$line"
		read -r line
done
exit 0



#5) numera entrada estandar
while read -r line
do
		echo "$num: $line"
		((num++))
done
exit 0






#4) processar entrada estandar(stdin) linea alinea
num=1
while read -r line
do
		echo "$num: $line"
		((num++))
done
exit 0


#3)itera per la llista d'arguments
while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0

# 2) comptador decreixent n(arg)...0
min=0
num=$1


while [ $num -ge $min ] 
do
	echo "$num" 
	((num--))
done 

exit 0


#1) mostrar un comptador del 1 al 10
max=10
num=1


while [ $num -le $max ] 
do
	echo "$num" 
	((num++))
done 

exit 0







