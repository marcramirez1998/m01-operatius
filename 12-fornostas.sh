#! /bin/bash
# @marcramirez1998
# Febrer 2023
# programa que rep unas notas i diu la nota (suspes,aprovat,notable,exelent)
# Exemples for notas
#----------------------------------------------------------

ERR_NARGS=1 

# 1) si num args no es correcte plegar 
if [ $# -eq 0 ] 
then   
	echo "ERROR: num args incorrecte"   
	echo "usage: $0 nota[...]"   
	exit $ERR_NARGS 
fi 
# 2) iterar per cada nota 
for nota in $* 
do   
	if ! [ $nota -ge 0 -a $nota -le 10 ]  
	then    
       		echo "Error: nota $nota no vàlida entre [0,10]" >> /dev/stderr  
	elif [ $nota -lt 5 ]   
	then    
       		echo "Suspès"  
	elif [ $nota -lt 7 ]   
	then    
       		echo "Aprovat"   
	elif [ $nota -lt 9 ]   
	then    
       		echo "Notable"   
	else     
		echo "Excel·lent"   
	fi
done 

exit 0







