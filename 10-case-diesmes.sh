#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple case: dias mes
#
#----------------------------------------------------------

#1) arguments incorrectes
ERR_NARGS=1
ERR_DIA=2

mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no existeix"
  echo "Els mesos son del 1 fins el 12"
  echo "Usage: $0 mes"
  exit $ERR_DIA
fi

if [ $# -ne 1 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 arguments"
	exit $ERR_NARGS

fi

#2) xixa
case $1 in
	"2")
		echo "el mes $1 te 28 dias"
		;;
	"4"|"6"|"9"|"11")
		echo "el mes $1 te 30 dias"
		;;
	*)
		echo "el mes $1 te 31 dias"
esac	
exit 0








