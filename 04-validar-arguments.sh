#! /bin/bash
# @marcramirez1998
# Febrer 2023
#
# Exemple if: indicar si és major d'edat
#	$prog edat
#----------------------------------------------------------

#1) arguments incorrectes
if [ $# -ne 2 ]
then
	echo "Error:numero arguments incorrecte"
	echo "Usage: $0 nom edat"
	exit 1
fi

#2) xixa
echo "nom: $1"
echo "edat: $2"
exit 0



